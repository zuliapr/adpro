package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

public class RoastedClam implements Clams {

    public String toString() {
        return "Roasted Clams from Chesapeake Bay";
    }
}
