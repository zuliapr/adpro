package id.ac.ui.cs.advprog.tutorial4.exercise1;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

public class FactoryTest {

    @Test
    public void PizzaTotalTest() {
        PizzaStore nyStore = new NewYorkPizzaStore();
        Pizza pizza = nyStore.orderPizza("cheese");

        assertEquals("---- New York Style Cheese Pizza ----\n"+
                "Thin Crust Dough\n"+
                "Marinara Sauce\n"+
                "Reggiano Cheese\n", pizza.toString());

    }
}
