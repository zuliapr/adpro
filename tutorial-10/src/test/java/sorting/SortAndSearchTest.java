package sorting;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import sorting.Sorter;

public class SortAndSearchTest {
    //TODO Implement, apply your test cases here
    private int[] arr1 = {1, 4, 2, 5, 3};
    private int[] arr3 = {3, 1, 2, 5, 4};

    @Test
    public void sortingTest() {
        Sorter.sort(arr1, 0, arr1.length-1);

        assertEquals(3, arr1[2]);
    }

    @Test
    public void finderTest() {
        int fast = Finder.binarySearch(arr3,0,arr3.length-1, 4);
        assertEquals(4, fast);

        int fast1 = Finder.binarySearch(arr3,0,arr3.length-1, 2);
        assertEquals(2, fast1);

    }

}
