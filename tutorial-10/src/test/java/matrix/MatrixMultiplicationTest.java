package matrix;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;


public class MatrixMultiplicationTest {
    //TODO Implement, apply your test cases here
    private static String genericMatrixPath = "plainTextDirectory/input/matrixProblem";
    private static String pathFileforMatrix1 = genericMatrixPath + "A/matrixProblemSet1.txt";
    private static String pathFileforMatrix2 = genericMatrixPath + "A/matrixProblemSet2.txt";

    private double[][] thenonSquareMatrix;
    private double[][] matrix1Tester;
    private double[][] matrix2Tester;

    @Before
    public void setUp() throws IOException {
        thenonSquareMatrix = new double[][]{{45, 67, 89, 100, 111}, {112, 56, 66, 88, 32}};
        matrix1Tester = Main.convertInputFileToMatrix(pathFileforMatrix1, 50, 50);
        matrix2Tester = Main.convertInputFileToMatrix(pathFileforMatrix2, 50, 50);
    }

    @Test
    public void testMainClassWorks() {
        assertTrue(mainClassChecker());
    }

    // Helper method for check the main class
    private static boolean mainClassChecker() {
        boolean exist = true;
        try {
            Main.main(null);
        } catch (Exception e) {
            exist = false;
        } finally {
            return exist;
        }
    }
}
