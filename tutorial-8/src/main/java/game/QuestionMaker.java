package game;

import java.util.Random;

public class QuestionMaker implements Runnable {
    private static final int QUEST_TYPE_ADD = 0;
    private static final int QUEST_TYPE_SUBSTR = 1;
    private static final int QUEST_TYPE_MULTIPL = 2;
    private static final int QUEST_TYPE_DIVS = 3;
    private Fraction expectedAnswer;

    public void run() {
        Random rand = new Random();
        Fraction firstPosFraction = new Fraction(rand.nextInt(40) - 20,
                rand.nextInt(40) - 20);
        Fraction secondPosFraction = new Fraction(rand.nextInt(40) - 20,
                rand.nextInt(40) - 20);


        switch (rand.nextInt(3)) {
            case QUEST_TYPE_ADD:
                System.out.print(firstPosFraction.toString() + "  +  "
                        + secondPosFraction.toString() + "  =  ");
                expectedAnswer = firstPosFraction.getAddition(secondPosFraction);
                break;
            case QUEST_TYPE_SUBSTR:
                System.out.print(firstPosFraction.toString() + "  -  "
                        + secondPosFraction.toString() + "  =  ");
                expectedAnswer = firstPosFraction.getSubstraction(secondPosFraction);
                break;
            case QUEST_TYPE_MULTIPL:
                System.out.print(firstPosFraction.toString() + "  *  "
                        + secondPosFraction.toString() + "  =  ");
                expectedAnswer = firstPosFraction.getMultiplication(secondPosFraction);
                break;
            case QUEST_TYPE_DIVS:
                System.out.print(firstPosFraction.toString() + "  :  "
                        + secondPosFraction.toString() + "  =  ");
                expectedAnswer = firstPosFraction.getDivision(secondPosFraction);
                break;
            default:
                System.out.println("Oooops!");
                expectedAnswer = new Fraction();
        }
    }

    public Fraction getExceptedAnswer() {
        return this.expectedAnswer;
    }
}
