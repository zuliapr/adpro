package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class NetworkExpert extends Employees {
    public NetworkExpert(String name, double salary) {
        this.name = name;
        this.salary = setSalary(salary);
        this.role = "Network Expert";
    }

    @Override
    public double getSalary() {
        return this.salary;
    }

    @Override
    public double setSalary(double salary) {
        if (salary < 50000) {
            throw new IllegalArgumentException();
        } else {
            return salary;
        }
    }
}
